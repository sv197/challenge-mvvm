//
//  NetworkManager.swift
//  Challenge MVVM
//
//  Created by The App Experts on 21/10/2020.
//  Copyright © 2020 The App Experts. All rights reserved.
//

import UIKit

class NetworkManager {
    
    //    var model = Model()
    var accounts: [AccountModel] = []
    
    //    func setText(completion: @escaping ((Model) -> ())) {
    //        self.model.text = "Accounts"
    //        completion(model)
    //    }
    
    func getAccountDataFromFile(completion: @escaping (([AccountModel]) -> ())) {
        guard let jsonData = readJsonFile(forName: "Accounts") else {
            return
        }
        parseJson(jsonData: jsonData)
        completion(accounts)
    }
    
    private func readJsonFile(forName name: String) -> Data? {
        do {
            if let path = Bundle.main.path(forResource: name,
                                           ofType: "json"),
                let jsonData = try String(contentsOfFile: path).data(using: .utf8) {
                return jsonData
            }
        } catch {
            print(error)
        }
        
        return nil
    }
    
    func setAccountDataFromServer(completion: @escaping (([AccountModel]) -> ())) {
        let urlString = "https://my-json-server.typicode.com/shanev96/challenge/db"
        loadJsonFrom(urlString: urlString) { (result) in
            switch (result) {
            case .success(let data):
                self.parseJsonFromServer(jsonData: data)
                completion(self.accounts)
            case .failure(let error):
                print(error)
            }
        }
    }
    
    private func loadJsonFrom(urlString: String, completion: @escaping (Result<Data, Error>) -> Void) {
        if let url = URL(string: urlString) {
            let urlSession = URLSession(configuration: .default).dataTask(with: url) { (data, response, error) in
                if let error = error {
                    completion(.failure(error))
                }
                
                if let data = data {
                    completion(.success(data))
                }
            }
            
            urlSession.resume()
        }
    }
    
    private func parseJson(jsonData: Data) {
        do {
            let decodedData = try JSONDecoder().decode([AccountModel].self,
                                                       from: jsonData)
            self.accounts = decodedData
        } catch {
            print("decode error")
        }
    }
    
    private func parseJsonFromServer(jsonData: Data) {
        do {
            let decodedData = try JSONDecoder().decode(RootObject.self,
                                                       from: jsonData)
            self.accounts = decodedData.accounts
        } catch {
            print("decode error")
        }
    }
    
}
