//
//  DetailViewController.swift
//  Challenge MVVM
//
//  Created by The App Experts on 23/10/2020.
//  Copyright © 2020 The App Experts. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet var accountTypeLabel: UILabel!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var accountNumberLabel: UILabel!
    @IBOutlet var currencyLabel: UILabel!
    @IBOutlet var balanceLabel: UILabel!
    
    @IBOutlet var tableView: UITableView!
    
    var viewModel: DetailViewModel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        callToViewModelForUIUpdate()
        self.setLabels()
        self.updateTable()
        tableView.tableFooterView = UIView()
        tableView.estimatedRowHeight = 160
    }

//    func callToViewModelForUIUpdate(){
//        self.viewModel = DetailViewModel()
//        self.viewModel.bindViewModelToController = {
//            self.setLabels()
//        }
//    }
    
    func updateTable(){
        DispatchQueue.main.async {
            self.tableView.delegate = self
            self.tableView.dataSource = self
            self.tableView.reloadData()
        }
    }
    
    func setLabels(){
        guard let account = viewModel.selectedAccountData else {
            return
        }
        self.accountTypeLabel?.text? += account.kind
        self.titleLabel?.text? += account.title
        self.accountNumberLabel?.text? += account.number
        self.currencyLabel?.text? += account.currency
        self.balanceLabel?.text? += String(account.balance)
    }
    
}

extension DetailViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.numberOfRowsInSection()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? TransactionTableViewCell else {
            return tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        }
        guard let transaction = viewModel.item(at: indexPath) else {
            return cell
        }
        cell.nameLabel.text? = "Name: \(transaction.name)"
        cell.amountLabel.text? += "Amount: \(transaction.amount)"
        cell.dateLabel.text? += "Date: \(transaction.date)"
        cell.statusLabel.text? += "Status: \(transaction.status)"
        cell.typeLabel.text? += "Type: \(transaction.type)"
        cell.sizeToFit()
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       return UITableView.automaticDimension
    }
    
}
