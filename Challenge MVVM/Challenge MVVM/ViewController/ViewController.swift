//
//  ViewController.swift
//  Challenge MVVM
//
//  Created by The App Experts on 21/10/2020.
//  Copyright © 2020 The App Experts. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var label: UILabel!
    @IBOutlet var tableView: UITableView!
    
    var viewModel: ViewModel!
    var selectedAccount = AccountModel()
    override func viewDidLoad() {
        super.viewDidLoad()
//        viewModel.getTextFromNetworkManager { (message) in
//            self.label.text = message.text
//        }
        callToViewModelForUIUpdate()
        tableView.tableFooterView = UIView()
    }
    
    func callToViewModelForUIUpdate(){
        self.viewModel = ViewModel()
        self.viewModel.bindViewModelToController = {
            self.updateTable()
        }
    }
    
    func updateTable(){
        DispatchQueue.main.async {
            self.tableView.delegate = self
            self.tableView.dataSource = self
            self.tableView.reloadData()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "showDetails") {
            guard let vc = segue.destination as? DetailViewController else {
                return
            }
            vc.viewModel = DetailViewModel(account: selectedAccount)
//            vc.viewModel.setAccountDetails(account: selectedAccount)
        }
    }
    
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.numberOfRowsInSection()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        guard let account = viewModel.item(at: indexPath) else {
            return cell
        }
        cell.textLabel?.text = account.title
        
        return cell

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let selected = viewModel.item(at: indexPath) {
            selectedAccount = selected
        }
        performSegue(withIdentifier: "showDetails", sender: self)
    }
    
}
