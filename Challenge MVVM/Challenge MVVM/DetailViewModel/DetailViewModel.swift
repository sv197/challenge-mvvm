//
//  DetailViewModel.swift
//  Challenge MVVM
//
//  Created by The App Experts on 23/10/2020.
//  Copyright © 2020 The App Experts. All rights reserved.
//

import UIKit

class DetailViewModel: NSObject {
    
//    var selectedAccount: Model!
//
//    init(selectedAccount: Model) {
//        self.selectedAccount = selectedAccount
//    }
    
//    func setAccountData(completion: @escaping ((Model)->())) {
//        completion(selectedAccount)
//    }
    
    
    private(set) var selectedAccountData : AccountModel! {
        didSet {
            self.bindViewModelToController()
        }
    }
    
    var bindViewModelToController : (() -> ()) = {}
    
//    func setAccountDetails(account: Model) {
//        selectedAccountData = account
//    }
    
    init(account: AccountModel) {
        super.init()
        setAccountData(account: account)
    }
    
    func setAccountData(account: AccountModel) {
        selectedAccountData = account
    }
}

extension DetailViewModel {
    var numberOfSections: Int {
        return 1 //return sections
    }
    
    func numberOfRowsInSection() -> Int {
        return selectedAccountData.transaction.count //use section too if needed
    }
    
    func item(at indexPath: IndexPath) -> Transaction? {
        if indexPath.section < 0 || indexPath.section > numberOfSections {
            return nil
        }
        if indexPath.row < 0 || indexPath.row > numberOfRowsInSection() {
            return nil
        }
        
        return selectedAccountData.transaction[indexPath.row]
    }
}
