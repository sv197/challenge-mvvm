//
//  ViewModel.swift
//  Challenge MVVM
//
//  Created by The App Experts on 21/10/2020.
//  Copyright © 2020 The App Experts. All rights reserved.
//

import UIKit

class ViewModel: NSObject {
    
    private var networkManager: NetworkManager!
    
    
    
//    func getTextFromNetworkManager(completion: @escaping ((Model) -> ())) {
//        networkManager.setText() { (message) in
//            print(message.text)
//            completion(message)
//        }
//    }
    
    private(set) var accountData : [AccountModel]! {
        didSet {
            self.bindViewModelToController()
        }
    }
    
    var bindViewModelToController : (() -> ()) = {}
    
    override init() {
        super.init()
        networkManager = NetworkManager()
        getAccountData()
    }
    
    func getAccountData() {
//        self.networkManager.getAccountDataFromFile { (accounts) in
////            print(accounts)
//            self.accountData = accounts
//        }
        self.networkManager.setAccountDataFromServer() { (accounts) in
            self.accountData = accounts
        }
        
    }
    
}

extension ViewModel {
    var numberOfSections: Int {
        return 1 //return sections
    }
    
    func numberOfRowsInSection() -> Int {
        return accountData.count //use section too if needed
    }
    
    func item(at indexPath: IndexPath) -> AccountModel? {
        if indexPath.section < 0 || indexPath.section > numberOfSections {
            return nil
        }
        if indexPath.row < 0 || indexPath.row > numberOfRowsInSection() {
            return nil
        }
        
        return accountData[indexPath.row]
    }
}
